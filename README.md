<h4>01 - Instalações necessarias</h4>
Faça o download do Python 3.8.x > https://www.python.org/ftp/python/3.8.6/python-3.8.6-amd64.exe<br>
<b>Instale via executável o Python 3.8. OBS.: Defina a variável de ambiente durante a instalação (recomendado).</b>

<h4>02 - Instalando o Robot Framework</h4>
No prompt de comando (cmder) execute e aguarde a instalação:<br>
pip install robotframework

<h4>instalação do chromedrive</h4>
Verifique em qual versão está o seu navegador<br>
Em seguida acesse > https://chromedriver.chromium.org/downloads <br>
<b>Faça o download da mesma versão que o seu navegador</b><br>
Em seguida deverá recortar o arquivo e colar na raiz do  diretório Windows<br>
exemplo > C:\Windows

<h4>03 - Rodando os testes</h4>
Atraves do Cmder: <br>
Acesse a pasta raiz do projeto<br>
Em seguida execulte o comando: robot -d ./logs tests\login.robot<br>
Será gerada a pasta logs com os reports do teste

