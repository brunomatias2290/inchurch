*** Settings ***

Documentation    Login




*** Keywords ***
Dado que acesso a página Login
    Pagina Login

Quando submeto minhas credencias
    [Arguments]                      ${email}             ${pass}

    Wait Until Element Is Visible    ${BTN_ENTRAR}

    Input Text                       ${LABEL_EMAIL}       ${email}
    Input Text                       ${LABEL_PASSWORD}    ${pass}

    Click Element                    ${BTN_ENTRAR}


Então devo ver a área logada
    Wait Until Element Is Visible    class:item