*** Variables ***
# Declaração da Variaveis

${email}                  desafio@qa.inchurch.com
${pass}                   qainchurch

${ENTAR_VISIT}            xpath://a[contains(text(),'Entrar como visitante')]
${NOTIFICACAO_CANCELD}    id:onesignal-slidedown-cancel-button
${MSG_ERROR}              xpath://h3[contains(text(),'Desculpe, ocorreu um erro.')]
${BTN_LOGIN}              xpath://a[@href='#/login']

${LABEL_EMAIL}            css:input[placeholder='Email']
${LABEL_PASSWORD}         css:input[placeholder='Password']
${BTN_ENTRAR}             css:button[name='myButton']

*** Keywords ***

Pagina Login
    # Acessa URL
    Go To                            ${url_login}

    # Aguarda até que o elemento X apareça
    Wait Until Element Is Visible    ${ENTAR_VISIT}
    # Clica no elemento
    Click Element                    ${ENTAR_VISIT}


    Wait Until Element Is Visible    ${NOTIFICACAO_CANCELD}
    Click Element                    ${NOTIFICACAO_CANCELD}
    
    Wait Until Element Is Visible    ${MSG_ERROR}
    Click Element                    ${MSG_ERROR}

    Wait Until Element Is Visible    ${BTN_LOGIN}
    Click Element                    ${BTN_LOGIN}
