**** Settings ***
Documentation    Login

Resource         ../resources/base.robot



# executa uma vez antes de todos os casos de testes
Suite Setup       Start Session


# execta uma única vez apos finalizar todos os casos de testes
Suite Teardown    Finish Session

*** Test Case ***
Login com sucesso
    Dado que acesso a página Login
    Quando submeto minhas credencias    ${email}    ${pass}
    Então devo ver a área logada